﻿using BBC_Final.Tools;
using TechTalk.SpecFlow;

namespace BBC_Final.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        [BeforeScenario]
        public void BeforeScenario()
        {
            WebDriver.Driver.Manage().Window.Maximize();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            WebDriver.Driver.Quit();
        }
    }
}
