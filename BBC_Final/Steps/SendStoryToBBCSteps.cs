<<<<<<< HEAD
﻿using BBC_Final.Pages;
=======
using BBC_Final.Pages;
>>>>>>> 5a9ec921461e8f11d9c3e5191affcc795ae3d739
using BBC_Final.Pages.HowYourShareToBBCPage;
using BBC_Final.Pages.Menu.MainMenu;
using BBC_Final.Pages.Menu.NewsMenu;
using BBC_Final.Tools;
using BBC_Final.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;

namespace BBC_Final.Steps
{
    [Binding]
    public class SendStoryToBbcSteps
    {
        private readonly IWebDriver driver = WebDriver.Driver;

        [When(@"I go to the How To Share BBC page")]
        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void WhenIGoToTheHowToShareBBCPage()
        {
            var mainMenu = new MainMenu(driver);
            var newsMenu = new NewsMenu(driver);
            var haveYourSayPage = new HaveYourSayPage(driver);
            mainMenu.ClickOnTheNewsMenuButton();
            newsMenu.ClickOnTheCoronavirusMenuButton();
            newsMenu.ClickOnTheYourCoronavirusStoriesSubMenuButton();
            haveYourSayPage.ClickOnTheHowToShareBBCNewsLink();
        }

        [When(@"I send story to BBC")]
        [Obsolete("ScenarioContext")]
        public void WhenISendStoryToBBC(Table table)
        {
            var sendStoryPage = new SendStoryPage(driver);
            sendStoryPage.FillAndSubmitForm(TableExtensions.ToDictionary(table));
            ScenarioContext.Current.Add("formElements", TableExtensions.ToDictionary(table).Keys.ToList());
        }

        [Then(@"Error message '(.*)' is displayed below field '(.*)'")]
        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void ThenErrorMessageIsDisplayedBelowField(string errorMessage, string field)
        {
            var sendStoryPage = new SendStoryPage(driver);
            Assert.IsTrue(sendStoryPage.GetValidationErrors(ScenarioContext.Current
                .Get<List<string>>("formElements"))[field.Trim('\"')] == errorMessage);
        }
    }
}