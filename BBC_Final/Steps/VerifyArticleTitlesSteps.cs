<<<<<<< HEAD
﻿using BBC_Final.Pages;
=======
using BBC_Final.Pages;
>>>>>>> 5a9ec921461e8f11d9c3e5191affcc795ae3d739
using BBC_Final.Pages.Menu.MainMenu;
using BBC_Final.Pages.NewsPage;
using BBC_Final.Tools;
using BBC_Final.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Linq;
using TechTalk.SpecFlow;

namespace BBC_Final.Steps
{
    [Binding]
    public class VerifyArticleTitlesSteps
    {
        private readonly IWebDriver driver = WebDriver.Driver;

        [Given(@"I am on the BBC Home page (.*)")]
        public void GivenIAmOnTheBBCHomePage(string homePageUrl)
        {
            var homePage = new HomePage(driver);
            homePage.OpenBBCHomePage(homePageUrl);
        }

        [When(@"I go to the News page")]
        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void WhenIGoToTheNewsPage()
        {
            var mainMenu = new MainMenu(driver);
            mainMenu.ClickOnTheNewsMenuButton();
        }

        [When(@"I search by the category of Top Story name")]
        public void WhenISearchByTheCategoryOfTopStoryName()
        {
            var mainMenu = new MainMenu(driver);
            mainMenu.EnterTheCategoryOfTopStoryInTheSearchBar();
            mainMenu.ClickOnTheSearchButton();
        }
        
        [Then(@"I see the headline article with title '(.*)'")]
        public void ThenISeeTheHeadlineArticleWithTitle(string title)
        {
            var newsTopStoriesBlock = new NewsTopStoriesBlock(driver);
            Assert.AreEqual(title, newsTopStoriesBlock.GetActualTextFromTheHeadlineArticle());
        }
        
        [Then(@"I see secondary articles with titles")]
        public void ThenISeeSecondaryArticlesWithTitles(Table table)
        {
            var newsTopStoriesBlock = new NewsTopStoriesBlock(driver);
            Assert.IsTrue(TableExtensions.ToList(table).SequenceEqual(newsTopStoriesBlock.GetActualTextFromSecondaryArticlesTitles()));
        }
        
        [Then(@"I see the first article with title '(.*)'")]
        public void ThenISeeTheFirstArticleWithTitle(string title)
        {
            var mainSearchResultPage = new MainSearchResultsPage(driver);
            Assert.AreEqual(title, mainSearchResultPage.ActualTextFromTheFirstArticle());
        }
    }
}