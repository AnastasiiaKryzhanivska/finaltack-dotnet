<<<<<<< HEAD
﻿using BBC_Final.Pages;
=======
using BBC_Final.Pages;
>>>>>>> 5a9ec921461e8f11d9c3e5191affcc795ae3d739
using BBC_Final.Pages.Menu.MainMenu;
using BBC_Final.Pages.Menu.SportMenu;
using BBC_Final.Pages.SearchMatchResults;
using BBC_Final.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace BBC_Final.Steps
{
    [Binding]
    public class FootballScoreSteps
    {
        private readonly IWebDriver driver = WebDriver.Driver;

        [When(@"I go to the Scores & Fixtures page")]
        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void WhenIGoToTheScoresFixturesPage()
        {
            var mainMenu = new MainMenu(driver);
            var sportMenu = new SportMenu(driver);
            mainMenu.ClickOnTheSportMenuButton();
            sportMenu.ClickOnTheFootballMenuButton();
            sportMenu.ClickOnTheScoresAndFixturesButton();
        }
        
        [When(@"I search the championship (.*) score in a specific month (.*)")]
        public void WhenISearchTheChampionshipScoreInASpecificMonth(string championship, string month)
        {
            var scoresAndFixturesPage = new ScoresAndFixturesPage(driver);
            scoresAndFixturesPage.SearchByChampionship(championship);
            scoresAndFixturesPage.ClickOnTheMonthButton(month);
        }

        [When(@"I click on the first championship match")]
        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void WhenIClickOnTheFirstChampionshipMatch()
        {
            var searchMatchResultPage = new SearchMatchResultPage(driver);
            searchMatchResultPage.ClickOnFirstChampionshipLink();
        }
        
        [Then(@"I see that the firstTeam (.*) and the secondTeam (.*) played with score (.*)")]
        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void ThenISeeThatTheFirstTeamAndTheSecondTeamPlayedWithScore(string firstTeam, string secondTeam, string score)
        {
            var matchDetailsBoard = new MatchDetailsBoard(driver);
            Assert.AreEqual(matchDetailsBoard.GetScore(firstTeam, secondTeam).GeneralTeamsScore, score);
        }
    }
}