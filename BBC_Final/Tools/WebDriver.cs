﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace BBC_Final.Tools
{
    public static class WebDriver
    {
        private static IWebDriver driver;

        public static IWebDriver Driver
        {
            get
            {
                if (driver == null)
                    driver = new ChromeDriver();
                return driver;
            }
        }
    }
}