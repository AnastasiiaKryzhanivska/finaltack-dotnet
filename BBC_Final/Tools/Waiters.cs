using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace BBC_Final.Tools
{
    public static class Waiters
    {
        private const int timeout = 60;

        public static void WaitForPageLoadingComplete()
        {
            new WebDriverWait(WebDriver.Driver, TimeSpan.FromSeconds(timeout)).Until(webDriver 
                => ((IJavaScriptExecutor)webDriver).ExecuteScript("return document.readyState").Equals("complete"));
        }

        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public static void WaitForElementToBeClickable(IWebElement element)
        {
            new WebDriverWait(WebDriver.Driver, TimeSpan.FromSeconds(timeout))
                .Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static void WaitVisibilityOfElement(By locator)
        {
            new WebDriverWait(WebDriver.Driver, TimeSpan.FromSeconds(timeout)).Until(SeleniumExtras.WaitHelpers
                .ExpectedConditions.ElementIsVisible(locator));
        }
    }
}