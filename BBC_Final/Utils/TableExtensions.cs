﻿using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;

namespace BBC_Final.Utils
{
    public static class TableExtensions
    {
        public static List<string> ToList(Table table)
            => table.Rows.Select(x => x.Values.ElementAt(0)).ToList();

        public static Dictionary<string, string> ToDictionary(Table table)
            => table.Rows.ToDictionary(x => x.Values.ElementAt(0).Trim('\"'), y => y.Values.ElementAt(1));
    }
}