Feature: Verify article titles


Scenario: Check the name of the headline article on the News page
	Given I am on the BBC Home page <"https://www.bbc.com">
	When I go to the News page
	Then I see the headline article with title 'Trump ends Covid budget stimulus relief talks'

Scenario: Check the name of the secondary articles on the News page
	Given I am on the BBC Home page <"https://www.bbc.com">
	When I go to the News page
	Then I see secondary articles with titles
	| Titles                                            |
	| Four Covid rules broken by the White House        |
	| Legendary guitarist Eddie Van Halen dies aged 65  |
	| Kremlin critic Navalny recalls near-death torment |
	| Trump's healthcare v the average American's       |
	| Facebook bans QAnon accounts across all platforms |

Scenario: Check the name of the first article after search by category
	Given I am on the BBC Home page <"https://www.bbc.com">
	When I go to the News page
	And I search by the category of Top Story name
	Then I see the first article with title 'US Election 2020: Your most pressing questions answered'