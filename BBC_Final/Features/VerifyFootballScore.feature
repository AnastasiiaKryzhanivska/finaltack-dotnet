Feature: Verify Football Score


Scenario: Check the score of the teams
	Given I am on the BBC Home page <"https://www.bbc.com">
	When I go to the Scores & Fixtures page
	And I search the championship <championship> score in a specific month <month>
	Then I see that the firstTeam <firstTeam> and the secondTeam <secondTeam> played with score <score>

	Examples: 
	| championship         | month   | firstTeam              | secondTeam           | score |
	| Championship         | 2020-09 | Bristol City           | Sheffield Wednesday  | 2:0   |
	| Premier League       | 2020-07 | Arsenal                | Watford              | 3:2   |
	| Scottish Premiership | 2020-08 | Celtic                 | Motherwell           | 3:0   |
	| Manchester United    | 2020-06 | Brighton & Hove Albion | Manchester United    | 0:3   |
	| National League      | 2020-02 | Barrow                 | Dagenham & Redbridge | 2:1   |
	
Scenario: Check the match score on Match Details Page
	Given I am on the BBC Home page <"https://www.bbc.com">
	When I go to the Scores & Fixtures page
	And I search the championship <championship> score in a specific month <month>
	And I click on the first championship match
	Then I see that the firstTeam <firstTeam> and the secondTeam <secondTeam> played with score <score>

	Examples: 
	| championship         | month   | firstTeam              | secondTeam           | score |
	| Championship         | 2020-09 | Bristol City           | Sheffield Wednesday  | 2:0   |
	| Premier League       | 2020-07 | Arsenal                | Watford              | 3:2   |
	| Scottish Premiership | 2020-08 | Celtic                 | Motherwell           | 3:0   |
	| Manchester United    | 2020-06 | Brighton & Hove Albion | Manchester United    | 0:3   |
	| National League      | 2020-02 | Barrow                 | Dagenham & Redbridge | 2:1   |