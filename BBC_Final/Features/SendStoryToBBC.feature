Feature: Send story to BBC


Scenario: Submit story to BBC leaving one of the mandatory fields empty
	Given I am on the BBC Home page <"https://www.bbc.com">
	When I go to the How To Share BBC page
	And I send story to BBC
	| field                  | value                  |
	| "Tell us your story. " | <Tell us your story. > |
	| Name                   | <Name>                 |
	| I accept the           | <I accept the>         |
	| I am over 16           | <I am over 16>         |
	Then Error message '<error message>' is displayed below field '<field>'

	Examples: 
	| Tell us your story. | Name | I accept the | I am over 16 | error message       | field                  |
	|                     | name | yes          | yes          | can't be blank      | "Tell us your story. " |
	| some story          |      | yes          | yes          | Name can't be blank | Name                   |
	| some story          | name | no           | yes          | must be accepted    | I accept the           |
	| some story          | name | yes          | no           | must be accepted    | I am over 16           |