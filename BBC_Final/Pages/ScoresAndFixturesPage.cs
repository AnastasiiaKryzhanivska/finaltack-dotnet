using BBC_Final.Tools;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Final.Pages
{
    public class ScoresAndFixturesPage : BasePage
    {
        private const string searchScoresAndFixturesInput = ".//input[@name='search']";
        private const string searchDropDown = "(.//a[@class = 'sp-c-search__result-item'])[1]";
        private const string firstPartXPathMonth = ".//a[contains(@href,'";
        private const string secondPartXPath = "')]";

        [FindsBy(How = How.XPath, Using = searchScoresAndFixturesInput)]
        public IWebElement SearchScoresAndFixturesInput { get; private set; }

        [FindsBy(How = How.XPath, Using = searchDropDown)]
        public IWebElement SearchDropDown { get; private set; }

        public ScoresAndFixturesPage(IWebDriver driver) : base(driver) { }

        public void SearchByChampionship(string championship)
        {
            SearchScoresAndFixturesInput.SendKeys(championship);
            SearchDropDown.Click();
        }

        public void ClickOnTheMonthButton(string month)
        {
            var monthElement = WebDriver.Driver.FindElement(By.XPath(firstPartXPathMonth
                                                                     + month
                                                                     + secondPartXPath));
            monthElement.Click();
            Waiters.WaitForPageLoadingComplete();
        }
    }
}