using BBC_Final.Tools;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;

namespace BBC_Final.Pages
{
    public class HaveYourSayPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = ".//a[contains(@href,'10725415')]")]
        public IWebElement HowToShareBBCNewsLink { get; private set; }

        public HaveYourSayPage(IWebDriver driver) : base(driver) { }

        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void ClickOnTheHowToShareBBCNewsLink()
        {
            Waiters.WaitForElementToBeClickable(HowToShareBBCNewsLink);
            HowToShareBBCNewsLink.Click();
            Waiters.WaitForPageLoadingComplete();
        }
    }
}