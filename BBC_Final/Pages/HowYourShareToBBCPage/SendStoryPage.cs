using OpenQA.Selenium;
using System.Collections.Generic;

namespace BBC_Final.Pages.HowYourShareToBBCPage
{
    public class SendStoryPage : BasePage
    {
        private readonly SendStoryForm StoryForm;

        public SendStoryPage(IWebDriver driver) : base(driver)
            => StoryForm = new SendStoryForm(driver);

        public void FillAndSubmitForm(Dictionary<string, string> values)
            => StoryForm.FillAndSubmitForm(values);

        public Dictionary<string, string> GetValidationErrors(List<string> formElements)
            => StoryForm.GetValidationErrors(formElements);
    }
}