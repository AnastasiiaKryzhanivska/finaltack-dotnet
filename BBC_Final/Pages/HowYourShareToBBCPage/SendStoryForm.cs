using BBC_Final.Tools;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using System.Linq;

namespace BBC_Final.Pages.HowYourShareToBBCPage
{
    public class SendStoryForm : BasePage
    {
        private readonly IWebDriver driver = WebDriver.Driver;
        private const string errorMessage = ".//div[@class='input-error-message']";

        [FindsBy(How = How.XPath, Using = ".//button[@class='button']")]
        public IWebElement SubmitButton { get; private set; }

        public SendStoryForm(IWebDriver driver) : base(driver) { }

        public void FillAndSubmitForm(Dictionary<string, string> credentials)
        {
            foreach (var element in credentials)
            {
                IList<IWebElement> textFields = driver.FindElements(By.XPath(".//*[@placeholder='"
                                                                             + element.Key
                                                                             + "']"));
                if (textFields.Count() != 0)
                    textFields[0].SendKeys(element.Value);
                else if (element.Value == "yes")
                    driver.FindElement(By.XPath(".//div[@class='checkbox']//p[contains(text(),'"
                                                + element.Key
                                                + "']")).Click();
            }
            SubmitButton.Click();
        }

        public Dictionary<string, string> GetValidationErrors(List<string> formElements)
        {
            Waiters.WaitVisibilityOfElement(errorMessage);
            var validationErrors = new Dictionary<string, string>();
            foreach (var element in formElements)
            {
                IList<IWebElement> inputFieldErrors = driver.FindElements(
                    By.XPath(".//*[@placeholder='"
                             + element
                             + "']/following-sibling::div[@class='input-error-message']"));
                if (inputFieldErrors.Count() != 0)
                    validationErrors.Add(element, inputFieldErrors[0].Text);
                else
                {
                    IList<IWebElement> checkboxErrors = driver.FindElements(
                        By.XPath(".//p[contains(text(),'"
                                 + element
                                 + "')]//ancestor::div[@class='checkbox']//div[@class='input-error-message']"));
                    if (checkboxErrors.Count() != 0)
                        validationErrors.Add(element, checkboxErrors[0].Text);
                }
            }
            return validationErrors;
        }
    }
}