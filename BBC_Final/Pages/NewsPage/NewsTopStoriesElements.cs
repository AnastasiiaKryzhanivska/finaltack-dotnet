using BBC_Final.Tools;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace BBC_Final.Pages.NewsPage
{
    public class NewsTopStoriesElements : NewsTopStoriesBlock
    {
        [FindsBy(How = How.XPath, Using = ".//div[contains(@class, 'inline-block@m')]//h3[contains(@class, 'promo-heading')]")]
        public IWebElement HeadlineArticle { get; private set; }

        [FindsBy(How = How.XPath, Using = ".//div[contains(@class,'secondary-item')]//h3[contains(@class,'heading')]")]
        public IList<IWebElement> SecondaryArticles { get; private set; }

        [FindsBy(How = How.XPath, Using = ".//div[contains(@class,'block@m')]//a[contains(@class,'section-link')]//span")]
        public IWebElement CategoryLink { get; private set; }

        public IWebElement Element { get; }

        public NewsTopStoriesElements(IWebElement element) : base(WebDriver.Driver) => Element = element;
    }
}