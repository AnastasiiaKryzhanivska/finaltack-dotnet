using BBC_Final.Tools;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace BBC_Final.Pages.NewsPage
{
    public class NewsTopStoriesBlock : BasePage
    {
        private readonly IWebDriver driver = WebDriver.Driver;
        private const string topStoriesContainer = ".//div[@id='news-top-stories-container']";

        public NewsTopStoriesElements TopStoriesContainer => new NewsTopStoriesElements(driver.FindElement(By.XPath(topStoriesContainer)));

        public NewsTopStoriesBlock(IWebDriver driver) : base(driver) { }

        public string GetActualTextFromTheHeadlineArticle() => TopStoriesContainer.HeadlineArticle.Text;

        public List<string> GetActualTextFromSecondaryArticlesTitles()
        {
            List<string> actualTextFromSecondaryArticlesTitles = new List<string>();
            foreach (var element in TopStoriesContainer.SecondaryArticles)
                actualTextFromSecondaryArticlesTitles.Add(element.Text);
            return actualTextFromSecondaryArticlesTitles;
        }

        public string GetNameOfTheCategory() => TopStoriesContainer.CategoryLink.Text;
    }
}