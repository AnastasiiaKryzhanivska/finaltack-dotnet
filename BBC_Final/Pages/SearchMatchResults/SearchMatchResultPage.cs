using BBC_Final.Tools;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;

namespace BBC_Final.Pages.SearchMatchResults
{
    public class SearchMatchResultPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "(.//span[contains(@class,'team-name--home')]//span[text()])[1]")]
        public IWebElement FirstMatch { get; private set; }

        public SearchMatchResultPage(IWebDriver driver) : base(driver) { }

        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void ClickOnFirstChampionshipLink()
        {
            Waiters.WaitForElementToBeClickable(FirstMatch);
            FirstMatch.Click();
        }
    }
}