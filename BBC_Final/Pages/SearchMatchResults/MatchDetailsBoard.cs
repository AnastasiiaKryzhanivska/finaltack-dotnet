using BBC_Final.Tools;
using BBC_Final.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;

namespace BBC_Final.Pages.SearchMatchResults
{
    public class MatchDetailsBoard : BasePage
    {
        [FindsBy(How = How.XPath, Using = "(.//div[contains(@class,'fixture__wrapper')])[1]")]
        public IWebElement MatchDataFirstBoard { get; private set; }

        public MatchDetailsBoard(IWebDriver driver) : base(driver) { }

        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public Score GetScore(string firstTeamName, string secondTeamName)
        {
            Waiters.WaitForElementToBeClickable(MatchDataFirstBoard);
            var firstTeamScore  = MatchDataFirstBoard.FindElement(
                By.XPath(".//span[contains(text(),'"
                         + firstTeamName
                         + "')]//ancestor::span[contains(@class,'team--home')]//span[contains(@class,'number')]")).Text;
            var secondTeamScore = MatchDataFirstBoard.FindElement(
                By.XPath(".//span[contains(text(),'"
                         + secondTeamName
                         + "')]//ancestor::span[contains(@class,'team--away')]//span[contains(@class,'number')]")).Text;
            return new Score(firstTeamScore, secondTeamScore);
        }
    }
}