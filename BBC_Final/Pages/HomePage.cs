﻿using BBC_Final.Tools;
using OpenQA.Selenium;

namespace BBC_Final.Pages
{
    public class HomePage : BasePage
    {
        public HomePage(IWebDriver driver) : base(driver) { }

        public void OpenBBCHomePage(string homePageUrl) => WebDriver.Driver.Navigate().GoToUrl(homePageUrl);
    }
}
