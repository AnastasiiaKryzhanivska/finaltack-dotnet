using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Final.Pages
{
    public class MainSearchResultsPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "(.//div[contains(@class, 'PromoContentSummary')]//a)[1]")]
        public IWebElement FirstNewsTitle { get; private set; }

        public MainSearchResultsPage(IWebDriver driver) : base(driver) { }

        public string ActualTextFromTheFirstArticle() => FirstNewsTitle.Text;
    }
}