using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Final.Pages
{
    public class BasePage
    {
        private readonly IWebDriver driver;

        [FindsBy(How = How.XPath, Using = ".//button[@class='sign_in-exit']")]
        public IWebElement MaybeLatterButton { get; private set; }

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void CloseTheSignInPopup()
        {
            if (MaybeLatterButton.Displayed)
                MaybeLatterButton.Click();
        }    
    }
}