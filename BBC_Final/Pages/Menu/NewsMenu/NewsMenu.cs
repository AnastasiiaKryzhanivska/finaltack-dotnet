using BBC_Final.Tools;
using OpenQA.Selenium;
using System;

namespace BBC_Final.Pages.Menu.NewsMenu
{
    public class NewsMenu : BasePage
    {
        private readonly IWebDriver driver = WebDriver.Driver;
        private const string newsMenuContainer = ".//header[@aria-label='news']";

        public NewsMenuElements NewsMenuContainer => new NewsMenuElements(driver.FindElement(By.XPath(newsMenuContainer)));

        public NewsMenu(IWebDriver driver) : base(driver) { }

        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void ClickOnTheCoronavirusMenuButton()
        {
            Waiters.WaitForElementToBeClickable(NewsMenuContainer.CoronavirusMenuButton);
            NewsMenuContainer.CoronavirusMenuButton.Click();
        }

        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void ClickOnTheYourCoronavirusStoriesSubMenuButton()
        {
            Waiters.WaitForElementToBeClickable(NewsMenuContainer.YourCoronavirusSubMenuButton);
            NewsMenuContainer.YourCoronavirusSubMenuButton.Click();
        }
    }
}