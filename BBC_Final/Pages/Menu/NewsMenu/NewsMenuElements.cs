using BBC_Final.Tools;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Final.Pages.Menu.NewsMenu
{
    public class NewsMenuElements : NewsMenu
    {
        [FindsBy(How = How.XPath, Using = ".//a[contains(@href, 'coronavirus')]")]
        public IWebElement CoronavirusMenuButton { get; private set; }

        [FindsBy(How = How.XPath, Using = ".//li[contains(@class, 'secondary')]/a[contains(@href, 'have_your_say')]")]
        public IWebElement YourCoronavirusSubMenuButton { get; private set; }

        public IWebElement Element { get; }

        public NewsMenuElements(IWebElement element) : base(WebDriver.Driver) => Element = element;
    }
}