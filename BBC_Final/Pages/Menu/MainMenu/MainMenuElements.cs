using BBC_Final.Tools;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Final.Pages.Menu.MainMenu
{
    public class MainMenuElements : MainMenu
    {
        [FindsBy(How = How.XPath, Using = ".//a[text()='News']")]
        public IWebElement NewsMenuButton { get; private set; }

        [FindsBy(How = How.XPath, Using = ".//a[text()='Sport']")]
        public IWebElement SportMenuButton { get; private set; }

        [FindsBy(How = How.XPath, Using = ".//input[@id='orb-search-q']")]
        public IWebElement SearchInput { get; private set; }

        [FindsBy(How = How.XPath, Using = ".//button[@id='orb-search-button']")]
        public IWebElement SearchButton { get; private set; }

        public IWebElement Element { get; }

        public MainMenuElements(IWebElement element) : base(WebDriver.Driver) => Element = element;
    }
}