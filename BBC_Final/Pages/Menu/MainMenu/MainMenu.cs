using BBC_Final.Pages.NewsPage;
using BBC_Final.Tools;
using OpenQA.Selenium;
using System;

namespace BBC_Final.Pages.Menu.MainMenu
{
    public class MainMenu : BasePage
    {
        private readonly IWebDriver driver = WebDriver.Driver;
        private const string mainMenuContainer = ".//div[@id='orb-header']";

        public MainMenuElements MainMenuContainer => new MainMenuElements(driver.FindElement(By.XPath(mainMenuContainer)));

        public MainMenu(IWebDriver driver) : base(driver) { }

        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void ClickOnTheNewsMenuButton()
        {
            Waiters.WaitForElementToBeClickable(MainMenuContainer.NewsMenuButton);
            MainMenuContainer.NewsMenuButton.Click();
            Waiters.WaitForPageLoadingComplete();
            CloseTheSignInPopup();
        }

        public void EnterTheCategoryOfTopStoryInTheSearchBar() 
            => MainMenuContainer.SearchInput.SendKeys(new NewsTopStoriesBlock(driver).GetNameOfTheCategory());

        public void ClickOnTheSearchButton()
        {
            MainMenuContainer.SearchButton.Click();
            Waiters.WaitForPageLoadingComplete();
        }

        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void ClickOnTheSportMenuButton()
        {
            Waiters.WaitForElementToBeClickable(MainMenuContainer.SportMenuButton);
            MainMenuContainer.SportMenuButton.Click();
            Waiters.WaitForPageLoadingComplete();
            CloseTheSignInPopup();
        }
    }
}