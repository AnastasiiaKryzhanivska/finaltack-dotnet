using BBC_Final.Tools;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Final.Pages.Menu.SportMenu
{
    public class SportMenuElements : SportMenu
    {
        [FindsBy(How = How.XPath, Using = ".//a[@data-stat-title='Football']")]
        public IWebElement FootballMenuButton { get; private set; }

        [FindsBy(How = How.XPath, Using = ".//a[@data-stat-title='Scores & Fixtures']")]
        public IWebElement ScoresAndFixturesButton { get; private set; }

        public IWebElement Element { get; }

        public SportMenuElements(IWebElement element) : base(WebDriver.Driver) => Element = element;
    }
}