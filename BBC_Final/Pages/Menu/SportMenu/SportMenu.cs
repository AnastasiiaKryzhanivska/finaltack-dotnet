using BBC_Final.Tools;
using OpenQA.Selenium;
using System;

namespace BBC_Final.Pages.Menu.SportMenu
{
    public class SportMenu : BasePage
    {
        private readonly IWebDriver driver = WebDriver.Driver;
        private const string sportMenuContainer = ".//div[@class='nav-top ']";

        public SportMenuElements SportMenuContainer => new SportMenuElements(driver.FindElement(By.XPath(sportMenuContainer)));

        public SportMenu(IWebDriver driver) : base(driver) { }

        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void ClickOnTheFootballMenuButton()
        {
            Waiters.WaitForElementToBeClickable(SportMenuContainer.FootballMenuButton);
            SportMenuContainer.FootballMenuButton.Click();
        }

        [Obsolete("ExpectedConditions.ElementToBeClickable()")]
        public void ClickOnTheScoresAndFixturesButton()
        {
            Waiters.WaitForElementToBeClickable(SportMenuContainer.ScoresAndFixturesButton);
            SportMenuContainer.ScoresAndFixturesButton.Click();
        }
    }
}